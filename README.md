
# Automação API Rest - Exemplo

Projeto template para testes de APIs Rest automatizados.


## API Reference

#### Post signin

```http
  POST /signin
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `User` | `user` | **Required**. Your API key |




## Authors

- [andersondacruzbenet@gmail.com](https://www.github.com/octokatherine)


## Running Tests

To run tests, run the following command

```bash
  mvn test
```


## Support

For support, e-mail andersondacruzbenet@gmail.com


## Tech Stack

<img src="images/docker.png" width=300 height=168 alt=1/>
<img src="images/restassured.png" width=306 height=164 alt=1/>
<img src="images/junit.png" width=224 height=224 alt=1/>
<img src="images/maven.png" width=225 height=225 alt=1/>
<img src="images/lombok.png" width=318 height=159 alt=1/>
<img src="images/docker.png" width=300 height=168 alt=1/>