package page;

import lombok.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {

    protected static final Logger logger = LogManager.getLogger();

    @EqualsAndHashCode.Include
    @NonNull
    private String email;

    @ToString.Exclude
    @EqualsAndHashCode.Include
    private String senha;

    @EqualsAndHashCode.Include
    @NonNull
    private boolean redirecionar;

    public static void main(String[] args) {


        User user1 = User.builder()
                .email("user1")
                .senha("123")
                .redirecionar(true)
                .build();

        System.out.println(user1);


    }

}
