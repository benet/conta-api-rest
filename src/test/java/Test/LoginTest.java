package Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import page.User;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;


public class LoginTest {
    final String BASE_URL = "https://barrigarest.wcaquino.me";
    User user;

    @BeforeEach
    public void beforeTest() {

        RestAssured.baseURI = BASE_URL;

        user = page.User.builder()
                .email("a@a")
                .senha("a")
                .redirecionar(true)
                .build();

    }


    @Test
    @Tag("health")
    @DisplayName("Health api validate ")
    public void healthTest() {
        user = page.User.builder()
                .email("a@a")
                .senha("b")
                .redirecionar(true)
                .build();

        given()
                .header("Content-type", "application/json")
                .when()
                .body(user)
                .post("/signin")
                .then()
                .statusCode(401);



    }

    @Test
    @Tag("contract")
    public void postContractLoginTest() {

        given()
                .header("Content-type", "application/json")
                .when()
                .body(user)
                .post("/signin")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("schemas/post_login.json"));


    }

    @Test
    @Tag("functional")
    public void successLoginTest() throws JsonProcessingException {

        Response response =
                given()
                        .header("Content-type", "application/json")
                        .when()
                        .body(user)
                        .post("/signin")
                        .then()
                        .assertThat()
                        .log().all()
                        .statusCode(200)
                        .body("id", equalTo(1))
                        .body("nome", equalTo("a"))
                        .extract().response();


    }


    @Test
    @Tag("functional")
    public void failLoginTest() throws JsonProcessingException {

        user = page.User.builder()
                .email("a@a")
                .senha("b")
                .redirecionar(true)
                .build();

        Response response =
                given()
                        .header("Content-type", "application/json")
                        .when()
                        .body(user)
                        .post("/signin")
                        .then()
                        .assertThat()
                        .log().all()
                        .statusCode(401)
                        .extract().response();

    }


}
